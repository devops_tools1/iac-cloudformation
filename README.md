# Iac Cloudformation

## Sobre el proyecto

> A continuación se presenta ejemplos básicos del uso de Cloudformation en donde le da un uso básico

## Prerequisitos

Lo siguientes requisitos son necesarias para poder probar los ejemplos:

* Una cuenta de AWS básica

## Ejemplos incluidos

Los ejemplos buscan dar una introducción a lo que es en realidad cloudformation, y alguna de sus características y porque se utiliza en AWS

### S3bucket

El ejemplo de S3 busca dar un introducción a como funcionan las plantillas de Cloudformation

#### ¿Qué es S3?

S3 es un servicio de almacenamiento que ofrece AWS, es utilizado para poder almacenar todo tipo de objetos (archivos, videos, imagenes, etc). Existen diferentes conceptos relacionados a S3:

* Buckets: son los contenedores principales de todos los objetos, estos deben de tener un nombre único en todas las cuentas de AWS
* Objetos: se les denomina a todos archivos o datos que se almacenan en un Bucket, estos pueden contener metadatos que contienen información importante.
* Llaves: son los nombres que tienen los objetos dentro de un Bucket

### ec2_Server

El ejemplo de EC2 tiene como propósito mostrar que es posible configurar un servidor web sencillo utilizando la plantillas de Cloudformation

#### ¿Qué es EC2?

EC2 es un servicio que permite crear máquinas virtuales en la nube, lo que ahorra costos relacionados a hardware. Existen distintos tipos de imagenes disponibles, este servicio puede escalar de forma vertical y horizontal dependiendo de las necesidades.

### Youtube Playlist

* [Playlist de video ejemplos](https://youtube.com/playlist?list=PL8Xnw9lMxPCH8jC310Utc774FQDHmm-MT)
